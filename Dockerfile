FROM docker:26.0

RUN <<EOF
    set -eux

    echo "**** upgrade current apk packages ****"
    apk -U --no-cache upgrade
    
    echo "**** install base deps ****"
    apk add --no-cache \
        musl \
        build-base \
        bash \
        curl \
        git \
        git-lfs \
        libffi-dev \
        openssl-dev \
        gcc \
        libc-dev \
        make \
        grep \
        jq

    echo "**** install Python ****"
    apk add --no-cache \
        python3 \
        python3-dev \
        py3-pip \
        py3-jsonschema \
        py3-requests \
        py3-cryptography \
        py3-pynacl \
        py3-yaml \
        py3-wheel \
        py3-setuptools

    if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi
    
    echo "**** install pip packages ****"

    pip3 install --no-cache --upgrade --break-system-packages \
        python-gitlab \
        pycparser
    
    chmod a+x /usr/bin/git-lfs
EOF

RUN mkdir -p $HOME/.docker

# end
